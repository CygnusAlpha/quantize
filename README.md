Quantize
========


[![Build Status](https://travis-ci.org/CustardCat/quantize.png)](https://travis-ci.org/CustardCat/quantize)


Adds a quantize() method to the Integer class. Floors any integer to the nearest lower multiple of the supplied number.

Briefly
-------

 require 'quantize'

 a = 15.quantize(10)

 puts a

10

Copyright
---------

Copyright (c) 2010 Bruce James. See LICENSE for details.

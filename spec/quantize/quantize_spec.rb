require 'quantize'

describe "quantize" do
    it "should round (0,10) to 0" do
        expect(0.quantize(10)).to eq(0)
    end

    it "should round(9,10) to 0" do
        expect(9.quantize(10)).to eq(0)
    end

    it "should round(10,10) to 10" do
        expect(10.quantize(10)).to eq(10)
    end

    it "should round(11,10) to 0" do
        expect(11.quantize(10)).to eq(10)
    end

    it "should round(19,10) to 10" do
        expect(19.quantize(10)).to eq(10)
    end
end
